# Importer en masse des photos d'élèves sur l'ENT Kosmos

**But** : Renommer les images en fonction d'un fichier CSV contenant les noms des élèves au format `nom.prenom`, compresser les images à une taille maximale de 50 Ko, et les organiser dans un dossier nommé `modifié`. Le tout sera ensuite archivé dans un fichier ZIP.

![Copie d'écran](import.jpg)

## Tutoriel

### Préparer les images

Assurez-vous que les images à renommer sont regroupées dans un seul dossier. Les noms de fichiers doivent correspondre aux élèves listés dans le fichier CSV.

### Préparer le fichier CSV

Le fichier CSV doit contenir une liste des élèves avec deux colonnes : ``nom` et prenom`. Assurez-vous d'utiliser la virgule comme séparateur.

### Lancer le programme `photokosmos.exe`

1. Cliquez sur **Renommer les photos**.
   - Sélectionnez le dossier contenant les photos.
   - Choisissez le fichier CSV correspondant.

2. Récupérez le fichier ZIP contenant les photos modifiées.

3. Importez le fichier ZIP sur l'ENT Kosmos.
   - Assurez-vous de choisir le format `NOM.prénom` pour l'importation.
