import os
import csv
import tkinter as tk
from tkinter import filedialog, messagebox
from PIL import Image, ExifTags
import shutil
import zipfile

def choisir_dossier():
    dossier = filedialog.askdirectory()
    if dossier:
        label_dossier.config(text=f"Dossier des photos : {dossier}")
    return dossier

def choisir_fichier():
    fichier = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")])
    if fichier:
        label_csv.config(text=f"Fichier CSV : {fichier}")
    return fichier

def sauvegarder_noms(originaux, dossier):
    """Sauvegarder les anciens noms de fichiers dans un fichier CSV pour pouvoir revenir en arrière."""
    backup_file = os.path.join(dossier, 'backup_noms_photos.csv')
    with open(backup_file, 'w', newline='', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['Ancien_nom', 'Nouveau_nom'])  # En-têtes des colonnes
        writer.writerows(originaux)
    print(f"Backup des noms enregistré dans {backup_file}")

def compresser_image(chemin_image, taille_max=50):
    """Compresser une image à une taille maximale donnée en Ko et conserver l'orientation."""
    image = Image.open(chemin_image)

    # Conserver l'orientation d'origine de l'image
    try:
        exif = image._getexif()
        if exif:
            for tag, value in exif.items():
                if tag in ExifTags.TAGS:
                    if ExifTags.TAGS[tag] == 'Orientation':
                        if value == 3:
                            image = image.rotate(180, expand=True)
                        elif value == 6:
                            image = image.rotate(270, expand=True)
                        elif value == 8:
                            image = image.rotate(90, expand=True)
                        break
    except AttributeError:
        pass

    nom_base, extension = os.path.splitext(chemin_image)
    extension = extension.lower()
    
    # Réduction de la taille physique de l'image si nécessaire
    largeur, hauteur = image.size
    ratio = min(1, (taille_max / (os.path.getsize(chemin_image) / 1024)) ** 0.5)  # Calculer un ratio de réduction

    if ratio < 1:
        image = image.resize((int(largeur * ratio), int(hauteur * ratio)), Image.LANCZOS)
        print(f"Réduit les dimensions de l'image à {image.size}")

    # Ajuster la qualité pour réduire la taille du fichier
    qualite = 95  # Démarrer avec une qualité élevée
    while qualite > 10:
        chemin_temp = nom_base + "_temp" + extension
        image.save(chemin_temp, quality=qualite, optimize=True)
        taille = os.path.getsize(chemin_temp) / 1024  # Taille en Ko
        if taille <= taille_max:
            break
        qualite -= 5
    
    # Remplacer l'image originale par l'image compressée
    os.replace(chemin_temp, chemin_image)
    print(f"Image compressée à {os.path.getsize(chemin_image) / 1024:.2f} Ko")

def zip_dossier(dossier_source, nom_zip):
    """Créer un fichier ZIP contenant le contenu du dossier source."""
    with zipfile.ZipFile(nom_zip, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, _, files in os.walk(dossier_source):
            for file in files:
                chemin_complet = os.path.join(root, file)
                # Ajouter le fichier au ZIP avec un chemin relatif
                arcname = os.path.relpath(chemin_complet, dossier_source)
                if not arcname.endswith(nom_zip):  # Exclure le fichier ZIP lui-même
                    zipf.write(chemin_complet, arcname=arcname)
    print(f"Archive ZIP créée : {nom_zip}")

def renommer_photos():
    try:
        # Sélectionner le dossier contenant les photos
        photos_path = choisir_dossier()
        if not photos_path:
            messagebox.showerror("Erreur", "Vous devez sélectionner un dossier de photos")
            return

        # Sélectionner le fichier CSV contenant la liste des élèves
        csv_file = choisir_fichier()
        if not csv_file:
            messagebox.showerror("Erreur", "Vous devez sélectionner un fichier CSV")
            return

        # Charger la liste des élèves depuis le fichier CSV
        with open(csv_file, newline='', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
            # Lire toutes les lignes du CSV pour compter le nombre d'élèves
            lignes_csv = list(reader)
            nb_eleves = len(lignes_csv)
            print(f"Nombre d'élèves dans le CSV : {nb_eleves}")

            # Lister tous les fichiers photos dans le dossier
            fichiers_photos = sorted(os.listdir(photos_path))
            # Filtrer uniquement les fichiers avec des extensions d'images (.jpg, .png, etc.)
            fichiers_photos = [f for f in fichiers_photos if f.lower().endswith(('.jpg', '.jpeg', '.png'))]
            print(f"Fichiers photos trouvés : {len(fichiers_photos)}")

            # Vérifier que le nombre de photos correspond au nombre d'élèves
            if len(fichiers_photos) != nb_eleves:
                messagebox.showerror("Erreur", f"Le nombre de photos ({len(fichiers_photos)}) ne correspond pas au nombre d'élèves ({nb_eleves}).")
                return

            # Nommer le dossier 'modifié' en fonction du dossier d'origine
            nom_dossier = os.path.basename(photos_path)
            dossier_modifie = os.path.join(os.path.dirname(photos_path), f"{nom_dossier}_modifié")
            os.makedirs(dossier_modifie, exist_ok=True)

            # Liste pour sauvegarder les noms de fichiers originaux et nouveaux
            anciens_noms = []

            # Copier, renommer, compresser et déplacer les photos une par une
            for i, row in enumerate(lignes_csv):
                prenom = row['prenom']
                nom = row['nom']

                # Nom de la photo actuelle
                ancien_nom_fichier = fichiers_photos[i]

                # Générer le nouveau nom de fichier en format nom.prenom
                extension = os.path.splitext(ancien_nom_fichier)[1]  # Récupérer l'extension (.jpg, .png, etc.)
                nouveau_nom_fichier = f"{nom}.{prenom}{extension}"

                # Chemins complets pour la copie et le renommage
                ancien_chemin = os.path.join(photos_path, ancien_nom_fichier)
                nouveau_chemin = os.path.join(dossier_modifie, nouveau_nom_fichier)

                # Copier le fichier dans le dossier modifié
                shutil.copy(ancien_chemin, nouveau_chemin)

                # Sauvegarder les anciens et nouveaux noms
                anciens_noms.append((ancien_nom_fichier, nouveau_nom_fichier))

                # Compresser l'image à une taille maximale de 50 Ko
                compresser_image(nouveau_chemin, taille_max=50)

            # Sauvegarder les noms d'origine pour une restauration potentielle
            sauvegarder_noms(anciens_noms, photos_path)

            # Créer un fichier ZIP contenant les photos modifiées dans le dossier modifié
            nom_zip = os.path.join(dossier_modifie, f"{nom_dossier}_modifié.zip")
            zip_dossier(dossier_modifie, nom_zip)

            messagebox.showinfo("Succès", f"Les photos ont été renommées, compressées et déplacées avec succès !\nArchive ZIP créée : {nom_zip}")

    except Exception as e:
        print(f"Erreur: {e}")
        messagebox.showerror("Erreur", str(e))

def restaurer_photos():
    """Restaurer les noms de fichiers à partir de la sauvegarde."""
    photos_path = choisir_dossier()
    if not photos_path:
        messagebox.showerror("Erreur", "Vous devez sélectionner le dossier des photos.")
        return

    backup_file = os.path.join(photos_path, 'backup_noms_photos.csv')
    if not os.path.exists(backup_file):
        messagebox.showerror("Erreur", f"Le fichier de sauvegarde {backup_file} est introuvable.")
        return

    # Restaurer les noms de fichiers
    try:
        with open(backup_file, newline='', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for row in reader:
                ancien_nom_fichier = row['Ancien_nom']
                nouveau_nom_fichier = row['Nouveau_nom']

                ancien_chemin = os.path.join(photos_path, 'modifié', nouveau_nom_fichier)
                nouveau_chemin = os.path.join(photos_path, ancien_nom_fichier)

                print(f"Restauré {nouveau_nom_fichier} en {ancien_nom_fichier}")
                os.rename(ancien_chemin, nouveau_chemin)

        messagebox.showinfo("Succès", "Les fichiers ont été restaurés avec succès !")

    except Exception as e:
        print(f"Erreur: {e}")
        messagebox.showerror("Erreur", str(e))

# Interface graphique
root = tk.Tk()
root.title("Renommer et restaurer les photos des élèves")

# Instructions
label_instruction1 = tk.Label(root, text="1. Choisir le dossier des photos", font=("Helvetica", 10))
label_instruction1.pack(pady=(10, 0))

# Étiquette pour afficher le dossier sélectionné
label_dossier = tk.Label(root, text="Dossier des photos : Aucun")
label_dossier.pack(pady=(0, 10))

# Instructions
label_instruction2 = tk.Label(root, text="2. Choisir le fichier CSV", font=("Helvetica", 10))
label_instruction2.pack(pady=(10, 0))

# Étiquette pour afficher le fichier CSV sélectionné
label_csv = tk.Label(root, text="Fichier CSV : Aucun")
label_csv.pack(pady=(0, 10))

# Créer des boutons pour démarrer le processus
button_renommer = tk.Button(root, text="Renommer les photos", command=renommer_photos)
button_renommer.pack(pady=10)

button_restaurer = tk.Button(root, text="Restaurer les noms originaux", command=restaurer_photos)
button_restaurer.pack(pady=10)

# Lancer la fenêtre principale
print("Lancement de la fenêtre tkinter")
root.mainloop()
